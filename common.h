#ifndef __COMMON_H__

//----- Type defines ----------------------------------------------------------
typedef unsigned char      byte;    // Byte is a char
typedef unsigned short	   word16;  // 16-bit word is a short int
typedef unsigned int       word32;  // 32-bit word is an int

	unsigned int crc32(unsigned int crc, const void *buf, size_t size);
	unsigned short checksum(unsigned char *addr, unsigned int count);
	unsigned int xor128(void);
#endif